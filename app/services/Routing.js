import React, { Component } from 'react';
import { DrawerNavigator,StackNavigator } from 'react-navigation';

// Components
import SplashScreen   from '../screens/SplashScreen';

import WelcomeScreen  from '../screens/Unauthorized/WelcomeScreen';
import SignInScreen   from '../screens/Unauthorized/SignInScreen';
import SignUpScreen   from '../screens/Unauthorized/SignUpScreen';

import HomeScreen     from '../screens/Authorized/HomeScreen';
import ProfileScreen  from '../screens/Authorized/ProfileScreen';

const UnauthorizedNavigator = StackNavigator(
  {
    Welcome: { 
      screen: WelcomeScreen 
    },
    Login: { 
      screen: SignInScreen 
    },
    SignUp: { 
      screen: SignUpScreen 
    },
  }, {
    initialRouteName: 'Welcome',
    headerMode: 'none',
  }
);

const AuthorizedNavigator = DrawerNavigator(
  {
    Home: {
      title: 'Inicio',
      screen: HomeScreen,
      path: '/',
      navigationOptions: {
        drawerLabel: 'Inicio',
        title: 'Inicio',
      }
    },
    Profile: {
      title: 'Cuenta',
      screen: ProfileScreen,
      path: '/account',
      navigationOptions: {
        drawerLabel: 'Mi Cuenta',
        title: 'Mi Cuenta',
      }
    }
  }
);

const Routing = StackNavigator(
  {
    Splash: { 
      screen: SplashScreen,
    },
    Unauthorized: { 
      screen: UnauthorizedNavigator 
    },
    Authorized: { 
      screen: AuthorizedNavigator 
    },
  }, {
    initialRouteName: 'Splash',
    headerMode: 'none'    
  }
);

export default Routing;