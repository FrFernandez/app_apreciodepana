import React, { Component } from 'react';
import { DrawerNavigator } from 'react-navigation';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Alert,
  StatusBar,
  TouchableNativeFeedback
} from 'react-native';
import { 
  Button,
  SocialIcon  
} from 'react-native-elements';
import { color } from 'react-native-material-design-styles';

export default class WelcomeScreen extends Component {
  constructor(props){
    super(props);
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={color.paperOrange300.color}
          barStyle="light-content"
          hidden={true}
        />
        <View style={styles.contentOmit}>
          <Button
            raised
            borderRadius={10}
            title="Omitir"
            underlayColor="#f3a646"
            buttonStyle={styles.btnOmit}
            onPress={() => navigate('Home')}
          />
        </View>
        <Image style={styles.Image} source={require('../../../resources/img/bill-ok.png')}/>
        <Text style={styles.title}>AprecioDePana</Text>
        <View style={styles.ButtonView}>
          <Button 
            large 
            raised
            title="Iniciar Sesión"
            underlayColor="#f3a646"
            buttonStyle={styles.Button}
            onPress={() => navigate('Login')}
          />
        </View>
        <View style={styles.ButtonView}>
          <Button 
            large 
            raised
            title="Registrate"
            underlayColor="#f3a646"
            buttonStyle={styles.Button}
            onPress={() => navigate('SignUp')}
          />
        </View>
        <View style={styles.contentSocial}>
          <SocialIcon
            raised
            type='google-plus-official'
            button
            onPress={this.loginWithGoogle}
            onLongPress={this.loginWithGoogle}
            style={styles.SocialIcons}
          />
          <SocialIcon
            raised
            type='facebook'
            button
            onPress={this.loginWithFacebook}
            onLongPress={this.loginWithFacebook}
            style={styles.SocialIcons}
          />
          <SocialIcon
            raised
            type='instagram'
            button
            onPress={this.loginWithInstagram}
            onLongPress={this.loginWithInstagram}
            style={styles.SocialIcons}
          />
        </View>
      </View>
    );
  }
  loginWithFacebook(){
    console.log('SignIn Facebook')
  }
  loginWithGoogle(){
    console.log('SignIn Google')
  }
  loginWithInstagram(){
    console.log('SignIn Instagram')
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color.paperOrange300.color,
  },
  Image: {
    resizeMode: 'stretch',
    width: 260,
    height: 260,
    zIndex: 2,
    marginTop: 16
  },
  title: {
    textAlign: 'center',
    fontSize: 26,
    color: '#FFF',
    marginTop: 16,
    marginBottom: 16,
  },
  Button: {
    backgroundColor: color.paperOrange300.color,
    height: 50,
    borderWidth: 0.5,
    borderColor: '#FFF',
  },
  ButtonView: {
    width: 240,
    marginTop: 10
  },
  contentSocial: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 10
  },
  SocialIcons: {
    width: 50,
    height: 50
  },
  btnOmit: {
    backgroundColor: color.paperOrange300.color,
    // borderWidth: 0.5,
    // borderColor: '#FFF',
  },
  contentOmit: {
    flex: 1,
    position: 'absolute',
    right: 0,
    top: 12,
    zIndex: 3,
  }
});
